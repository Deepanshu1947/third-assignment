/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ProducerConsumer.main;

import com.ProducerConsumer.Conventions.Conventions;
//import java.util.ArrayList;

import com.ProducerConsumer.Entities.*;
import java.util.Scanner;

public class ProducerConsumer  implements Conventions {

    public static  int count;

    
    public static void main(String[] args) {

        Thread producer = new Thread(new Producer());
        Thread consumer = new Thread(new Consumer());
        producer.start();
        consumer.start();
        Scanner input = new Scanner(System.in);
        input.nextLine();
        Producer.producerShutDown();
        Consumer.consumerShutDown();
    }

}
