package com.ProducerConsumer.Entities;

import com.ProducerConsumer.Conventions.Conventions;
import static com.ProducerConsumer.main.ProducerConsumer.count;

public class Producer  implements Runnable, Conventions {
 public static Boolean running=true;
    @Override
    public void run() {
            while (running) {
           synchronized (lock) {
             if (count < MAX_SIZE && count >= MIN_SIZE) {
                    System.out.println("Produced: " + count);
                    count++;
                    lock.notifyAll();
                } else {
                    try {
                        lock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
    
   public static void producerShutDown()
    {
        running =false;
    }
}
