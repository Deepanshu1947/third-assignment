/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ProducerConsumer.Entities;

import static com.ProducerConsumer.main.ProducerConsumer.count;

public class Consumer  implements Runnable, com.ProducerConsumer.Conventions.Conventions {
static Boolean running=true;
    @Override
    public void run() {
            while (running) {
                
        synchronized (lock) {
                if (count <= MAX_SIZE && count > MIN_SIZE) {
                    System.out.println("consumed: " + count);
                    count--;
                    lock.notify();
                } else {
                    try {
                        lock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

   public static void consumerShutDown()
    {
        running =false;
    }
}
